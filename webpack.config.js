/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');

const setupDevServer = require('./config/setupDevServer');
const processScripts = require('./config/processScripts');
const processStyles = require('./config/processStyles');
const processImages = require('./config/processImages');
const processFonts = require('./config/processFonts');
const generatePlugins = require('./config/generatePlugins');

const themePath = path.resolve(__dirname, './').replace(path.resolve(__dirname, '../../'), '');


module.exports = function bundle(env) {
  return {
    mode: env.mode === 'development' ? 'development' : 'production',
    // mode: 'development',
    entry: {
      // app: './src/js/app.js',
      app: './src/ts/index.ts',
    },
    // devtool: env.mode === 'development' ? 'eval' : 'source-map',
    devtool: env.mode === 'development' ? false : 'source-map',
    output: {
      path: path.resolve(__dirname, 'assets/'),
      filename: `js/${env.mode === 'legacy' ? '[name]--legacy' : '[name]'}.js`,
      publicPath: `.${themePath}/assets/`,
    },
    module: {
      rules: [
        //   {
        //   test: /\.ttf|eot|woff|woff2$/i,
        //   use: [{
        //     loader: 'file-loader',
        //     options: {
        //       name: 'fonts/[name].[ext]',
        //     },
        //   }],
        // },
        processFonts(env.mode),
        processImages(env.mode),
        processStyles(env.mode),
        ...processScripts(env.mode),
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: generatePlugins(env.mode),
    watch: env.mode === 'development',
    watchOptions: {
      ignored: /node_modules/,
    },
    devServer: setupDevServer(env.mode),
  };
};