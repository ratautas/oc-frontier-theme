interface WindowWithFB extends Window {
  fbAsyncInit: any;
  FB: any;
}

(window as WindowWithFB).fbAsyncInit = function() {
  (window as WindowWithFB).FB.init({
    appId: '173486693602143',
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v3.2',
  });
};

(function(d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js';
  fjs.parentNode.insertBefore(js, fjs);
})(document, 'script', 'facebook-jssdk');
