import CustomEase from '../vendor/gsap/CustomEase';

export default function cubicBezier(id: string, fn: string) {
  return CustomEase.create(id, fn);
}
