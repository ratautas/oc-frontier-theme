if (typeof _nc === 'undefined') {
  var _nc = {};
}

// export const device = {
let device = {
  isSet: false,
  isPortrait: screen.width < screen.height,
  ratio: screen.width / screen.height,
  isTouch: typeof window.ontouchstart !== 'undefined',
  isMob: screen.width < 768,
  isTablet: screen.width < 1025 && screen.width > 767,
  isLaptop: screen.width > 1024,
  winW: window.innerWidth,
  winH: window.innerHeight,
  bodyW: document.body.clientWidth,
  bodyH: document.body.clientHeight,
  scrollbarW: window.innerWidth - document.body.clientWidth,
  hasScrollbar: document.body.clientWidth !== window.innerWidth,
  isIOS: ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0,
  OS: null,
  ua: window.navigator.userAgent,
};

if (device.ua.indexOf('Windows NT 10.0') != -1) device.OS = 'win-10';
if (device.ua.indexOf('Windows NT 6.2') != -1) device.OS = 'win-8';
if (device.ua.indexOf('Windows NT 6.1') != -1) device.OS = 'win-7';
if (device.ua.indexOf('Windows NT 5.1') != -1) device.OS = 'win-xp';
if (device.ua.indexOf('Linux') != -1) device.OS = 'linux';
if (device.ua.indexOf('Mac') != -1) device.OS = 'mac-os';

document.body.classList.add(device.isIOS ? 'is-ios' : 'no-ios');
document.body.classList.add(device.isTouch ? 'is-touch' : 'no-touch');
document.body.classList.add(device.isMob ? 'is-mob' : 'no-mob');
document.body.classList.add(device.isTablet && device.isTouch ? 'is-tablet' : 'no-tablet');
document.body.classList.add(device.isLaptop ? 'h' : 'no-laptop');
document.body.classList.add(device.OS);
document.body.classList.add(
  !!device.ua.match(/Version\/[\d\.]+.*Safari/) && device.isTouch ? 'mob-safari' : 'no-safari',
);

if (device.ua.indexOf('Windows') != -1) document.body.classList.add('has-scrollbar');

(device as any).check = function() {
  device.isSet = false;
  device.winW = window.innerWidth;
  device.winH = window.innerHeight;
  device.bodyW = document.body.clientWidth;
  device.bodyH = document.body.clientHeight;
  device.ratio = window.innerWidth / window.innerHeight;
  device.scrollbarW = window.innerWidth - document.body.clientWidth;
  device.hasScrollbar = document.body.clientWidth !== window.innerWidth;
};

(device as any).set = function() {
  if (!device.isSet) {
    device.isSet = true;
    setTimeout(function() {
      (device as any).check();
    }, 100);
  }
};
