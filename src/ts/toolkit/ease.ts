export const easeIn = (p: number) => (t: number) => Math.pow(t, p);
export const easeOut = (p: number) => (t: number) => 1 - Math.abs(Math.pow(t - 1, p));
export const easeInOut = (p: number) => (t: number) =>
  t < 0.5 ? easeIn(p)(t * 2) / 2 : easeOut(p)(t * 2 - 1) / 2 + 0.5;

// https://gist.github.com/gre/1650294
// USAGE: power = 3, t = 0.5
// const t = easeInOut(3)(0.5);
