export default function removeLocationHash() {
  // const noHashURL = window.location.href.replace(/#.*$/, '');
  // window.history.replaceState('', document.title, noHashURL);
  window.history.replaceState(null, null, location.href.replace(/#.*$/, ''));
}
