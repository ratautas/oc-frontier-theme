import getTransitionDuration from '../toolkit/getTransitionDuration';

const defaultTime = 500;

document.addEventListener(
  'enter',
  (e: CustomEvent) => {
    const { $dispatcher } = e.detail;
    const time: number = e.detail.time || getTransitionDuration($dispatcher);

    $dispatcher.classList.add('will-enter');

    setTimeout(() => $dispatcher.classList.add('is-entering'), 1);
    setTimeout(() => $dispatcher.classList.remove('will-enter'), 1);

    setTimeout(() => {
      $dispatcher.classList.add('has-entered');
      setTimeout(() => $dispatcher.classList.remove('is-entering'), 1);
    }, time);
  },
  false,
);

document.addEventListener(
  'leave',
  (e: CustomEvent) => {
    const { $dispatcher } = e.detail;
    const time: number = e.detail.time || getTransitionDuration($dispatcher);

    $dispatcher.classList.remove('has-entered');
    $dispatcher.classList.add('will-leave');

    setTimeout(() => $dispatcher.classList.add('is-leaving'), 1);
    setTimeout(() => $dispatcher.classList.remove('will-leave'), 1);

    setTimeout(() => $dispatcher.classList.remove('is-leaving'), time);
  },
  false,
);

export function enter($dispatcher: Element | HTMLElement, time?: number) {
  document.dispatchEvent(
    new CustomEvent('enter', { detail: { $dispatcher, time: time || defaultTime } }),
  );
}

export function leave($dispatcher: Element | HTMLElement, time?: number) {
  document.dispatchEvent(
    new CustomEvent('leave', { detail: { $dispatcher, time: time || defaultTime } }),
  );
}

export default function transition($el: Element | HTMLElement, time?: number) {
  if ($el) {
    $el.matches('.has-entered') ? leave($el, time || defaultTime) : enter($el, time || defaultTime);
  } else {
    console.error('bad element was provided for transition');
  }
}
