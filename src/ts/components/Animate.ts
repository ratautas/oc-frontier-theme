// interface HTMLElementWithTimeline extends HTMLElement {
//   timeline: any;
// }
import $$ from '../toolkit/$$';

export default class Animate {
  $$animatables: NodeListOf<HTMLElement> = $$('[data-animate]');
  $logo: HTMLElement = $$('[data-logo]')[0];
  $heroIntro: HTMLElement = $$('[data-hero-intro]')[0];
  animations: any = {};

  constructor() {
    [].forEach.call(this.$$animatables, $animatable => {
      let delay = 0;
      const offset = 100;
      const html = $animatable.innerText.split('').reduce((html, letter) => {
        return (html += '<span>' + letter + '</span>');
      }, '');
      $animatable.innerHTML = html;
      this.animations[$animatable.dataset.animate] = () => {
        [].forEach.call($animatable.getElementsByTagName('span'), ($span: HTMLElement) => {
          setTimeout(() => $span.classList.add('is-ready'), delay);
          delay += offset;
        });
      };
    });
  }

  public start() {
    this.$heroIntro.classList.add('is-animating');
    this.animations['hi'].call();
    setTimeout(() => this.$logo.classList.add('is-ready'), 1600);
    setTimeout(() => this.animations['im'].call(), 1200);
    setTimeout(() => this.animations['coming'].call(), 2800);
  }
}
