interface WindowWithFB extends Window {
  fbAsyncInit: any;
  FB: any;
}

import 'webrtc-adapter';
import $$ from '../toolkit/$$';
import 'tracking/build/tracking-min';
import 'tracking/build/data/face-min';
import '../misc/fb.ts';
import serialize from '../toolkit/serialize';

navigator.getUserMedia =
  navigator.getUserMedia ||
  navigator['webkitGetUserMedia'] ||
  navigator['mozGetUserMedia'] ||
  navigator['msGetUserMedia'];

// // console.log(FB);

// window['fbAsyncInit'] = function() {
//   window['FB'].init({
//     appId: '173486693602143',
//     autoLogAppEvents: true,
//     xfbml: true,
//     version: 'v3.2',
//   });
// };

class Tracker {
  $video: HTMLVideoElement = document.getElementById('video') as HTMLVideoElement;
  $canvas: HTMLCanvasElement = document.getElementById('canvas') as HTMLCanvasElement;
  $upload: HTMLElement = document.getElementById('upload');
  $download: HTMLElement = document.getElementById('download');
  $success: HTMLElement = document.getElementById('success');
  $image: HTMLImageElement = document.getElementById('image') as HTMLImageElement;
  $file: HTMLInputElement = document.getElementById('file') as HTMLInputElement;
  $form: HTMLFormElement = document.getElementById('addpage-base64') as HTMLFormElement;
  $$hats: NodeListOf<HTMLElement> = $$('[data-hat]');
  $share: HTMLElement = $$('[data-share]')[0];
  $$steps: NodeListOf<HTMLElement> = $$('[data-capture-step]');
  $captureStart: HTMLElement = $$('[data-capture-start]')[0];
  $captureRetake: HTMLElement = $$('[data-capture-retake]')[0];
  $captureUpload: HTMLButtonElement = $$('[data-capture-upload]')[0];
  $captureConsent: HTMLInputElement = $$('[data-capture-consent]')[0];
  hatImg: HTMLImageElement = document.createElement('img');
  hatImgUrl: string;
  hatImgRatio: number;
  pageTitle: string = '';
  base64: string = '';
  width: number = 0;
  height: number = 0;
  isTracking: boolean = false;
  rX: number = 100;
  rY: number = 10;
  rW: number = 500;
  rH: number = 250;
  stream: any = null;
  streamVideoTrack: any;
  shareBaseUrl: string;
  controller: any;
  context: CanvasRenderingContext2D;
  trackingTask: any;
  tracking: any;
  constructor() {
    this.tracking = window['tracking'];
    this.hatImgUrl = this.$$hats[0].dataset.hat;
    this.shareBaseUrl = this.$share.dataset.share;
    this.controller = new this.tracking.ObjectTracker('face');
    this.context = (this.$canvas as HTMLCanvasElement).getContext('2d');

    [].forEach.call(this.$$hats, ($hat: HTMLElement) => {
      $hat.addEventListener('click', () => {
        this.hatImgUrl = $hat.dataset.hat;
        this.start();
      });
    });

    this.$captureStart.addEventListener('click', () => this.capture());

    this.$captureRetake.addEventListener('click', () => {
      this.isTracking = true;
      this.context.clearRect(0, 0, this.width, this.height);
      this.drawHat();
      this.setStep('1');
      this.$video.play();
      this.trackingTask.run();
    });

    this.$captureConsent.addEventListener('change', () => {
      this.$captureUpload.disabled = !this.$captureConsent.checked;
    });

    this.$captureUpload.addEventListener('click', () => {
      this.pageTitle = Date.now().toString();
      $$('[name="data[title]"]')[0].value = this.pageTitle;
      $$('[name="data[base64]"]')[0].value = this.base64;

      fetch(this.$form.action, {
        method: this.$form.method,
        body: serialize(this.$form),
        headers: {
          Accept: 'text/html, */*; q=0.01',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
      })
        .then(res => res.text())
        .then(() => {
          // this.tracking.stopUserMedia();
          this.streamVideoTrack.stop();
          this.$success.setAttribute('src', this.base64);
          this.setStep('3');
        });
    });

    this.$share.addEventListener('click', () => {
      (window as WindowWithFB).FB.ui(
        {
          method: 'share_open_graph',
          action_type: 'og.shares',
          action_properties: JSON.stringify({
            object: {
              'og:title': 'test',
              'og:image': this.shareBaseUrl + '/' + this.pageTitle + '/capture.png',
              'og:description': 'description',
            },
          }),
        },
        function(response) {},
      );
    });
  }

  start() {
    this.init();
    this.setStep('1');
    return this;
  }

  init() {
    var self = this;
    this.controller.setInitialScale(8);
    this.controller.setStepSize(1);
    this.controller.setEdgesDensity(0.1);
    this.hatImg.src = this.hatImgUrl;
    this.hatImgRatio = this.hatImg.width / this.hatImg.height;
    if (navigator.mediaDevices.getUserMedia) {
      this.$video.onplay = () => {
        if (!this.isTracking) {
          this.width = this.$video.clientWidth;
          this.height = this.$video.clientHeight;
          this.$canvas.setAttribute('width', this.width.toString());
          this.$canvas.setAttribute('height', this.height.toString());
          this.isTracking = true;
          this.trackVideo();
        }
      };

      navigator.mediaDevices
        .getUserMedia({
          audio: false,
          video: true,
        })
        .then(stream => {
          this.stream = stream;
          this.streamVideoTrack = stream.getTracks()[0];
          this.$video.srcObject = stream;
        })
        .catch(function(error) {
          // $('[data-capture-step]').removeClass('is-active');
          // $('[data-capture-step="error"]').addClass('is-active');
          console.error(error.name + ': ' + error.message);
        });
    } else {
      if (window['File'] && window['FileReader'] && window['FormData']) {
        // $('#file').click();
        this.$file.click();
        this.$file.addEventListener('change', (e: any) => {
          const file = e.target.files[0];
          if (file && /^image\//i.test(file.type)) {
            const reader: any = new FileReader();
            reader.onloadend = () => {
              this.$image.setAttribute('src', reader.result);
              if (!self.isTracking) {
                this.width = this.$image.width;
                this.height = this.$image.height;
                this.$canvas.setAttribute('width', this.width.toString());
                this.$canvas.setAttribute('height', this.height.toString());
                this.isTracking = true;
                this.trackImage();
              }
            };
            reader.onerror = function() {
              alert('There was an error reading the file!');
            };
            reader.readAsDataURL(file);
          } else {
            alert('Not a valid image!');
          }
        });
      } else {
        alert('File upload is not supported:(');
      }
    }
    return this;
  }

  drawHat() {
    this.context.drawImage(this.hatImg, this.rX, this.rY, this.rW, this.rH);
  }

  /**
   * Loops through all the steps removes class `.is-active`, unless `data-capture-step`
   * attribute matches passed `id`.
   *
   * @param {string} id
   * @memberof Tracker
   */
  setStep(id: string) {
    [].forEach.call(this.$$steps, ($step: HTMLElement) => {
      $step.classList[$step.dataset['captureStep'] == id ? 'add' : 'remove']('is-active');
    });
  }

  trackImage() {
    var self = this;
    this.trackingTask = this.tracking.track('#image', this.controller);
    this.drawHat();
    this.controller.on('track', (e: any) => {
      if (e.data.length) {
        this.context.clearRect(0, 0, this.width, this.height);
        e.data.forEach(rect => {
          var w = rect.width * 1.7;
          var h = w / this.hatImgRatio;
          this.rW = w;
          this.rH = h;
          this.rX = rect.x - 80;
          this.rY = rect.y - h + 80;
          this.drawHat();
        });
      }
    });
  }

  trackVideo() {
    this.$video.src = '';
    this.trackingTask = this.tracking.track('#video', this.controller, {
      camera: true,
    });
    this.drawHat();
    this.hatImgRatio = this.hatImg.width / this.hatImg.height;
    this.controller.on('track', e => {
      if (e.data.length) {
        this.context.clearRect(0, 0, this.width, this.height);
        e.data.forEach((rect: any) => {
          const w = rect.width * 1.7;
          const h = w / this.hatImgRatio;
          this.rW = w;
          this.rH = h;
          this.rX = rect.x - 80;
          this.rY = rect.y - h + 80;
          this.drawHat();
        });
      }
    });
  }

  capture() {
    this.isTracking = false;

    var videoCover = this.cover(
      this.$canvas.width,
      this.$canvas.height,
      this.$video.videoWidth,
      this.$video.videoHeight,
    );

    this.context.drawImage(
      this.$video,
      videoCover.offsetX,
      videoCover.offsetY,
      videoCover.width,
      videoCover.height,
    );

    this.drawHat();
    this.base64 = this.$canvas.toDataURL('image/jpeg');
    this.$upload.setAttribute('src', this.base64);
    this.$download.setAttribute('href', this.base64);
    this.setStep('2');
    this.trackingTask.stop();
    this.$video.pause();
  }

  cover(parentWidth, parentHeight, childWidth, childHeight, offsetX?, offsetY?) {
    offsetX = typeof offsetX !== 'undefined' ? offsetX : 0.5;
    offsetY = typeof offsetY !== 'undefined' ? offsetY : 0.5;
    var childRatio = childWidth / childHeight;
    var parentRatio = parentWidth / parentHeight;
    var width = parentWidth;
    var height = parentHeight;
    if (childRatio < parentRatio) {
      height = width / childRatio;
    } else {
      width = height * childRatio;
    }
    return {
      width,
      height,
      offsetX: (parentWidth - width) * offsetX,
      offsetY: (parentHeight - height) * offsetY,
    };
  }
}

export default Tracker;
