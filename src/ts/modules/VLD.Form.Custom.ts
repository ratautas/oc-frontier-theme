import VLDForm from './VLD.Form';

class VLDFormCustom extends VLDForm {
  constructor($form: HTMLFormElement) {
    super($form); // <-- EXTEND CONSTRUCTOR
    console.log('an example how to exend a VLDForm constructor');
  }
  public beforeFieldsMount() {
    super.beforeFieldsMount(); // <-- EXTEND METHOD
    console.log('an example how to EXTEND a VLDForm method while keeping previous functionality');
  }
  public onMount() {
    console.log('an example how to OVERRIDE a VLDForm method');
  }
}

export default VLDFormCustom;
