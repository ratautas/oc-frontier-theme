import lodashThrottle from 'lodash.throttle';

import Parallax from './Parallax';

import $$ from '../toolkit/$$';

/**
 * ParallaxsController initializer factory.
 *
 * @module ParallaxsController
 */

class ParallaxsController {
  /**
   * Creates an instance of ParallaxsController.
   *
   * @memberof ParallaxsController
   * @returns An object of initialized ParallaxsController.
   */

  // A NodeList of '[data-parallax]' HTML Elements:
  public $parallaxs: NodeListOf<HTMLElement>;

  // An array of mounted parallaxs.
  public parallaxs: any[];

  public activeTrackers: any = {};

  /**
   * Creates an instance of ParallaxsController.
   * If there are any `[data-parallax]` in DOM, call `mountParallaxs()`.
   *
   * @memberof ParallaxsController
   */
  constructor() {
    this.$parallaxs = $$('[data-parallax]');
    if (this.$parallaxs[0]) this.mountParallaxs();
  }

  /**
   * Loops through all `$parallaxs` and depending on type, call Parallax constructors.
   *
   * @returns {ParallaxsController} For chaining metods
   * @memberof ParallaxsController
   */
  mountParallaxs() {
    this.beforeMount();

    this.parallaxs = [].map.call(this.$parallaxs, ($parallax, i) => {
      // const watcher = scrollmonitor.create(parallax.$parallax, -30);
      // watcher.enterViewport(() => this.onParallaxEnter(parallax, watcher));
      return new Parallax($parallax, this, i);
    });

    window.addEventListener(
      'scroll',
      lodashThrottle(() => {
        console.log(this.activeTrackers);
        // console.log('scroll');
        for (const parallax in this.activeTrackers) {
          this.activeTrackers[parallax].seek();
        }
        // if (this.inViewPort) {
        //   console.log('in viewport..');
        // }
      }, 10),
      { capture: false, passive: true },
    );

    this.afterMount();

    return this;
  }

  /**
   * Extra function to call BEFORE mounting all parallaxs.
   *
   * @returns {ParallaxsController} For chaining.
   * @memberof ParallaxsController
   */
  public beforeMount() {
    return this;
  }

  /**
   * Extra function to call AFTER mounting all parallaxs.
   *
   * @returns {ParallaxsController} For chaining.
   * @memberof ParallaxsController
   */
  public afterMount() {
    return this;
  }

  /**
   *
   * @returns {ParallaxsController} For chaining.
   * @memberof ParallaxsController
   */
  public onParallaxEnter(parallax, watcher) {
    // watcher.destroy();
    // this.parallaxsQueue.push(parallax);
    // if (this.parallaxsQueue.length === 1) this.enqueueTween(parallax);
    return this;
  }

  public addTracker(parallax) {
    this.activeTrackers[`tracker${parallax.i}`] = parallax;
  }

  public removeTracker(parallax) {
    delete this.activeTrackers[`tracker${parallax.i}`];
  }
}

export default ParallaxsController;
