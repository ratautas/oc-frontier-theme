import { TimelineLite, CSSPlugin, TweenMax, Linear } from 'gsap/all';
import scrollmonitor from 'scrollmonitor';
import lodashThrottle from 'lodash.throttle';

import kebabize from '../toolkit/kebabize';
// prevent tree shaking:
const gsapPlugins = [CSSPlugin];

class Parallax {
  // Initialized parallax.
  public parallax: any;
  // Initialized timeline.
  public timeline: any;
  // Parallax's travel. If negative, it moves to opposite direction.
  public travel: number = -100;
  // Enter/exit watcher, initialized with scrollominitor
  public watcher: any = null;
  // Start positon
  public start: number = 0;
  // End positon
  public end: number = 0;
  // Paralax element's height
  public height: number = 0;
  // Is element in viewPort?
  public inViewPort: boolean = false;

  public $parallax: HTMLElement;
  public parallaxsController: any;
  public i: number;

  /**
   * Creates an instance of Parallax. Assigns class variables and iniaitate Swiper module.
   * @param {HTMLElement} $parallax HTML element of the Parallax's target;
   * @param {*} parallaxsController Parallax's parent initialization funtion
   * @param {number} i Parallax's initializator's place in array
   * @memberof Parallax
   */
  constructor($parallax: HTMLElement, parallaxsController: any, i: number) {
    this.parallaxsController = parallaxsController;
    this.i = i;
    this.$parallax = $parallax;
    this.travel = $parallax.dataset.parallax ? Number($parallax.dataset.parallax) : this.travel;

    this.onScroll = this.onScroll.bind(this);

    this.mountParallax();

    // window.addEventListener(
    //   'scroll',
    //   lodashThrottle(() => {
    //     console.log('scroll');
    //     if (this.inViewPort) {
    //       console.log('in viewport..');
    //     }
    //   }, 60),
    // );
  }

  /**
   * Start mounting lifecycle - beforeMount, onMount and afterMount hooks.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public mountParallax() {
    this.beforeMount();
    this.calculatePosition();
    this.onMount();
    this.afterMount();
    return this;
  }

  /**
   * An actual animation function (GSAP, anime etc) initialization.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public calculatePosition() {
    const rect = this.$parallax.getBoundingClientRect();
    const top = rect.top + window.scrollY;
    this.height = rect.height;
    this.start = top < window.innerHeight ? top + window.innerHeight : top;
    this.end = top + rect.height;
    // this.inViewPort = this.start < window.scrollY + window.innerHeight;
    return this;
  }

  /**
   * An actual animation function (GSAP, anime etc) initialization.
   * Initially set to paused, but should be overriten in every class extensio.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public onMount() {
    this.timeline = new TimelineLite({
      paused: true,
      ease: Linear.easeNone,
    });

    const duration = this.height + this.travel + window.innerHeight;

    this.timeline.to(this.$parallax, duration, { y: this.travel });

    this.watcher = scrollmonitor.create(this.$parallax, 0);
    // this.watcher.enterViewport(() => window.addEventListener('scroll', this.onScroll, false));
    this.watcher.enterViewport(() => {
      this.inViewPort = true;
      this.parallaxsController.addTracker(this);
    });
    this.watcher.exitViewport(() => {
      this.inViewPort = false;
      this.parallaxsController.removeTracker(this);
    });
    return this;
  }

  public seek() {
    const progress = window.scrollY + window.innerHeight - this.start;
    this.timeline.seek(window.scrollY + window.innerHeight - this.start);
    // console.log(this.timeline);
  }

  public onScroll() {
    this.timeline.seek(window.scrollY + window.innerHeight - this.start);
    // console.log(this.timeline);
  }

  /**
   * Extra function to call BEFORE mounting parallax.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public beforeMount() {
    return this;
  }

  /**
   * Extra function to call AFTER mounting parallax.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public afterMount() {
    return this;
  }

  /**
   * Extra function to call before opening parallax.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public beforeStart() {
    return this;
  }

  /**
   * Extra function to call after opening parallax.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public afterStart() {
    return this;
  }

  /**
   * Remove added styles and data-parallax attriute from element
   *
   * @param {HTMLElement} $el An element from which styles shall be removed.
   * @param {string[]} attributes An array of CSS properties to be removed.
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public cleanUp($el: HTMLElement, props: string[]) {
    if (!this.parallaxsController.isTweening) {
      if ($el) {
        props.forEach(prop => ($el.style[prop === 'x' || prop === 'y' ? 'transform' : prop] = ''));
        if (Object.keys($el.dataset).length) {
          for (const key in $el.dataset) {
            if (key.includes('parallax')) $el.removeAttribute(`data-${kebabize(key)}`);
          }
        }
        $el.style['will-change'] = '';
        if (!$el.getAttribute('style')) $el.removeAttribute('style');
      }
    } else {
      setTimeout(() => this.cleanUp($el, props), 500);
    }
    return this;
  }

  /**
   * Callback after `Parallax.timeline` is complete.
   * Used for extending in child classes for additional individual cleanup.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public onEnd() {
    return this;
  }

  /**
   * Callback function, which is called u[on Parallax.timeline completion.
   * Executes some additional code cleanup and emits `onParallaxEnd()` to parent controller.
   *
   * @returns {Parallax} For chaining.
   * @memberof Parallax
   */
  public onTimelineComplete() {
    console.log('onTimelineComplete');
    return this;
  }
}

export default Parallax;
