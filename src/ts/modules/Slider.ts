class Slider {
  // Initialized slider
  public slider: any;
  // Sensible defauts;
  public options: { [key: string]: any };
  // HTML element of the root of the Slider;
  public $target: HTMLElement;
  // HTML element of Slider's bullets;
  public $bullets: NodeListOf<HTMLElement>;
  // currently active slide number
  public activeSlide: number;

  public $slider: HTMLElement;
  public slidersController: any;
  public i: number;

  /**
   * Creates an instance of Slider. Assigns class variables and iniaitate Swiper module.
   * @param {HTMLElement} $slider HTML element of the Slider's target;
   * @param {*} swiperModule Imported Swiper module
   * @param {*} slidersController Slider's parent initialization funtion
   * @param {number} i Slider's initializator's place in array
   * @memberof Slider
   */
  constructor($slider: HTMLElement, swiperModule: any, slidersController: any, i: number) {
    this.slidersController = slidersController;
    this.i = i;
    this.$slider = $slider;
    this.$target = $slider.querySelector('[data-slider-target]');
    this.$bullets = $slider.querySelectorAll('[data-slider-bullet]');

    this.slider = new swiperModule(this.$target, this.getOptions());

    this.afterMount();
  }

  /**
   * Extra function to call after initialization.
   *
   * @returns {Slider} For chaining.
   * @memberof Slider
   */
  public afterMount() {
    if (this.$bullets[0]) this.addBullets();
    return this;
  }

  /**
   * Add custom bullets and hook them into Swiper events.
   *
   * @returns {Slider} For chaining.
   * @memberof Slider
   */
  public addBullets() {
    [].forEach.call(this.$bullets, ($bullet, b) => {
      $bullet.addEventListener('click', () => {
        const to = this.options.loop ? b + 1 : b;
        this.slider.slideTo(to, this.options.speed);
        [].forEach.call(this.$bullets, ($b: HTMLElement) => {
          $b.classList[$b === $bullet ? 'add' : 'remove']('is-active');
        });
      });
    });

    this.slider.on('transitionEnd', () => {
      this.activeSlide = this.slider.realIndex;
      [].forEach.call(this.$bullets, ($bullet, b) => {
        $bullet.classList[b === this.slider.realIndex ? 'add' : 'remove']('is-active');
      });
    });

    return this;
  }

  /**
   * Set default options and assign overrides/custom options from `updateOptions()`.
   *
   * @returns Swiper options object
   * @memberof Slider
   */
  public getOptions() {
    const options = {
      loop: true,
      speed: 400,
      simulateTouch: false,
    };
    return (this.options = Object.assign(options, this.updateOptions()));
  }

  /**
   * Additional options/overrides - to be used when extending.
   *
   * @returns Swiper options object
   * @memberof Slider
   */
  public updateOptions() {
    // const options = {
    //   speed: 2000,
    // };
    //
    // return options;
  }
}

export default Slider;
