import $$ from '../toolkit/$$';
import lodashThrottle from 'lodash.throttle';

class HeaderController {
  public currentPosition: number = window.scrollY;
  public lastPosition: number = null;
  public speed: number = 0;
  public scrollDown: boolean = true;
  public headerUp: boolean = false;
  public headerNarrow: boolean = false;
  public scrollTimer: any;

  public options: {
    // Any extra options
    [option: string]: any;
  };
  constructor(options?: any) {
    this.options = options ? { ...options } : {};
    this.track = this.track.bind(this);
    window.addEventListener('scroll', lodashThrottle(this.track, 60));
  }
  public track(e) {
    if (this.lastPosition !== null) this.speed = this.currentPosition - this.lastPosition;
    this.lastPosition = this.currentPosition;
    this.scrollDown = this.speed <= 0 || this.currentPosition === 0;
    clearTimeout(this.scrollTimer);
    this.scrollTimer = setTimeout(() => {
      this.lastPosition = null;
      this.speed = 0;
    }, 50);
    this.currentPosition = window.scrollY;
    if (this.headerUp && this.speed < 0) {
      this.headerUp = false;
      document.body.classList.remove('header-up');
    } else if (!this.headerUp && this.speed > 0) {
      this.headerUp = true;
      document.body.classList.add('header-up');
    }
    if (!this.headerNarrow && this.currentPosition > 1) {
      this.headerNarrow = true;
      document.body.classList.add('header-narrow');
    } else if (this.headerNarrow && this.currentPosition <= 1) {
      this.headerNarrow = false;
      document.body.classList.remove('header-narrow');
    }
  }
}

export default HeaderController;
