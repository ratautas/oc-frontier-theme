// import Modal from './Modal';
import ModalCapture from './Modal.Capture';
import ModalAlex from './Modal.Alex';

import $$ from '../toolkit/$$';
import { fixBody, releaseBody } from '../toolkit/fixBody';

/**
 * ModalsController initializer factory.
 *
 * @module ModalsController
 * @requires ModalsInstance
 */

class ModalsController {
  /**
   * Creates an instance of ModalsController.
   *
   * @memberof ModalsController
   * @returns An object of initialized ModalsController.
   */

  // A NodeList of '[data-modal]' HTML Elements:
  public $modals: NodeListOf<HTMLElement>;

  // An array of mounted modals.
  public modals: any[];

  // An array of active modals in a queue.
  public activeModalsQueue: any[] = [];

  // Active modal object.
  public activeModal: any = null;

  // ID of an active modal
  public activeModalID: string = null;

  // Root url.
  public rootUrl: string = document.body.getAttribute('data-root');

  public options: {
    // Mode, default to `hash`, so URLS are generated with "#". Can be `href`.
    urlMode?: string;
    // If modals should layer on top or should close automatically before opening.
    closePrevious?: boolean;
  };

  /**
   * Creates an instance of ModalsController.
   * If there are any `[data-modal]` in DOM, call `mountModals()`.
   *
   * @memberof ModalsController
   */
  constructor(options?) {
    this.options = options ? { urlMode: 'hash', ...options } : { urlMode: 'hash' };
    this.$modals = $$('[data-modal]');
    if (this.$modals[0]) this.mountModals();
  }

  /**
   * Loops through all `$modals` and depending on type, call Modal constructors.
   *
   * @returns {ModalsController} For chaining metods
   * @memberof ModalsController
   */
  mountModals() {
    this.modals = [].map.call(this.$modals, ($modal, i) => {
      const id = $modal.getAttribute('data-modal');
      if (id === 'capture') return new ModalCapture($modal, this, i);
      return new ModalAlex($modal, this, i);
    });

    document.addEventListener('keyup', (e: any) => {
      if (e.key === 'Escape' && this.activeModal) this.activeModal.close();
    });

    this.checkForID();

    this.afterMount();

    return this;
  }

  /**
   * Extra function to call after mounting all modals.
   *
   * @returns {ModalsController} For chaining.
   * @memberof ModalsController
   */
  public afterMount() {
    return this;
  }

  /**
   * A callback function fired just after modal.open() function.
   * Takes in passed down modal object. Controls hash/href.
   *
   * @param {*} modal A mounted modal object.
   * @returns {Modal} For chaining.
   * @memberof Modal
   */
  public onModalOpen(modal) {
    // If there were no modals open currently (queue is empty), fix body.
    if (this.activeModalsQueue.length === 0) fixBody();
    if (!this.options.closePrevious) {
      // If options.closePrevious is NOT set or set to FALSE, add target modal to the queue.
      this.activeModalsQueue.push(modal);
      // Else if options.closePrevious is set to TRUE and there is an other currently active modal.
    } else if (this.activeModal) {
      // Close previously active modal.
      this.activeModal.close();
    }
    // Assign activeModal as target modal.
    this.activeModal = modal;
    // Assign activeModalID as target modal's ID.
    this.activeModalID = modal.id;
    // Update URL.
    this.updateUrl();
    return this;
  }

  /**
   * A callback function fired just after modal.close() function.
   * Controls hashe/href.
   *
   * @returns {Modal} For chaining.
   * @memberof Modal
   */
  public onModalClose() {
    // First, remove last modal from the queue.
    this.activeModalsQueue.pop();
    if (this.activeModalsQueue.length > 0) {
      // If there are still modals in queue, get the last modal and assding current id.
      this.activeModal = this.activeModalsQueue[this.activeModalsQueue.length - 1];
      // Assign activeModalID as new acticeModal's ID.
      this.activeModalID = this.activeModal.id;
      // Update URL.
      this.updateUrl();
    } else {
      this.activeModal = null;
      this.activeModalID = '';
      history.pushState(null, null, this.rootUrl);
      releaseBody();
    }
    return this;
  }

  /**
   * Check URL for modal's ID and opens it, if found.
   *
   * @returns {ModalsController} For chaining.
   * @memberof ModalsController
   */
  public checkForID() {
    // Exctract needed strings.
    let { href } = window.location;
    const { hash } = window.location;
    // If the last symobol of current URL is '/', remove it.
    if (href[href.length - 1] === '/') href = href.slice(0, -1);
    // If urlMode is 'href', get the last piece of current URL from '/'.
    // Else, if it is hash, just remove '#'.
    const slug = this.options.urlMode === 'href' ? href.split('/').pop() : hash.replace('#', '');
    // Search throught all the modal with '.find()', to find matching 'id'.
    const targetModal = this.modals.find(modal => modal.id === slug);
    // If target moadl was found, open it.
    if (targetModal) targetModal.open();

    return this;
  }

  /**
   * Update URL.
   *
   * @returns {ModalsController} For chaining.
   * @memberof ModalsController
   */
  private updateUrl() {
    if (this.options.urlMode === 'href') {
      // If URL mode is set to 'href', use history API.
      history.pushState(null, null, `${this.rootUrl}/${this.activeModalID}`);
    } else {
      // If URL mode is NOT set to set to 'hash', add hash.
      location.hash = `#${this.activeModalID}`;
    }
    return this;
  }
}

export default ModalsController;
