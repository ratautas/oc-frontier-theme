import Modal from './Modal';

class ModalAlex extends Modal {
  /**
   * Extending `beforeOpen()` method.
   */
  public beforeOpen() {
    // If there was a modal open previously, disable transitions from all the modals.
    // Else, return to normal state :)
    [].forEach.call(this.modalsController.$modals, ($modal: HTMLElement) => {
      $modal.classList[this.modalsController.activeModal ? 'add' : 'remove']('no-transition');
    });
    return this;
  }
}

export default ModalAlex;
