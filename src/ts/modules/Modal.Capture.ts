import ModalAlex from './Modal.Alex';

import Tracker from '../components/Tracker';

class ModalCapture extends ModalAlex {
  //
  tracker: any = new Tracker();

  /**
   * Extending `beforeOpen()` method.
   */
  public beforeOpen() {
    this.tracker.start();
    return this;
  }

  /**
   * Extending `afterClose()` method.
   */
  public afterClose() {
    if (this.tracker) {
      if (this.tracker.isTracking) {
        if (this.tracker.streamVideoTrack) this.tracker.streamVideoTrack.stop();
        this.tracker.isTracking = false;
        this.tracker.$video.pause();
      }
    }
    return this;
  }
}

export default ModalCapture;
