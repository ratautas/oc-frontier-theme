import $$ from '../toolkit/$$';

class Tween {
  // Initialized tween.
  public tween: any;
  // Tween active triggers.
  public $triggers: NodeListOf<HTMLElement>;
  // Tween close triggers.
  public $closes: NodeListOf<HTMLElement>;
  // Tween's unique ID.
  public id: string = '';
  // Tween's state, not active by default.
  public isActive: boolean = false;

  public $tween: HTMLElement;
  public tweensController: any;
  public i: number;

  /**
   * Creates an instance of Tween. Assigns class variables.
   * @param {HTMLElement} $tween HTML element of the Tween's target;
   * @param {*} tweensController Tween's parent initialization funtion
   * @param {number} i Tween's initializator's place in array
   * @memberof Tween
   */
  constructor($tween: HTMLElement, tweensController: any, i: number) {
    this.tweensController = tweensController;
    this.i = i;
    this.$tween = $tween;
    this.id = $tween.dataset.tween;

    this.beforeMount();

    this.addTriggerEvents();

    this.afterMount();
  }

  /**
   * Get all triggers and add event listeners to them.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public addTriggerEvents() {
    this.$triggers = $$(`[data-tweener-trigger="${this.id}"]`);
    this.$closes = this.$tween.querySelectorAll('[data-tweener-close]');

    if (this.$triggers[0]) {
      [].forEach.call(this.$triggers, ($trigger: HTMLElement) => {
        $trigger.addEventListener('click', () => this.open());
      });
    }

    if (this.$closes[0]) {
      [].forEach.call(this.$closes, ($close: HTMLElement) => {
        $close.addEventListener('click', () => this.close());
      });
    }

    return this;
  }

  /**
   * Open tween. Fired on `$triggers` click or from `TweenController`.
   * Emit `onTweenOpen()` on parent controller with itself as parameter.
   * Update current tween - add classes, animate, play with inner HTML here upon opening.
   * `beforeOpen()` and `afterOpen()` ar more like modifiers to be extended in custom tweens.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public open() {
    this.beforeOpen();

    if (!this.isActive) {
      this.tweensController.onTweenOpen(this);
      this.$tween.classList.add('is-active');
    }

    this.afterOpen();

    return this;
  }

  /**
   * Close tween. Fired on `$closes` click or from `TweenController`.
   * Emit `onTweenClose()` on parent controller with itself as parameter.
   * Update current tween - add classes, animate, play with inner HTML here upon closing.
   * `beforeClose()` and `afterClose()` ar more like modifiers to be extended in custom tweens.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public close() {
    this.beforeClose();

    this.tweensController.onTweenClose(this);
    this.$tween.classList.remove('is-active');

    this.afterClose();

    return this;
  }

  /**
   * Extra function to call before mounting tween.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public beforeMount() {
    return this;
  }

  /**
   * Extra function to call after mounting tween.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public afterMount() {
    return this;
  }

  /**
   * Extra function to call before opening tween.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public beforeOpen() {
    return this;
  }

  /**
   * Extra function to call after opening tween.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public afterOpen() {
    return this;
  }

  /**
   * Extra function to call before closing tween.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public beforeClose() {
    return this;
  }

  /**
   * Extra function to call after closing tween.
   * Used too hook in extra functionality when extending.
   *
   * @returns {Tween} For chaining.
   * @memberof Tween
   */
  public afterClose() {
    return this;
  }
}

export default Tween;
