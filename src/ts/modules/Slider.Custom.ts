import Slider from './Slider';

class SliderCustom extends Slider {
  /**
   * Extra function to call after initialization.
   * In this example we slow down transitions and log some methods.
   *
   * @returns {SliderCustom} For chaining.
   * @memberof SliderCustom
   */
  public afterMount() {
    super.afterMount(); // <-- EXTEND METHOD
    console.log('An additional action after mount');
    return this;
  }

  /**
   * Additional options/overrides - to be used when extending.
   * In the example we'll just speed up.
   *
   * @returns Swiper options object
   * @memberof SliderCustom
   */
  public updateOptions() {
    const options = {
      speed: 100,
    };

    return options;
  }
}

export default SliderCustom;
