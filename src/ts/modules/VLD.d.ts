// Type definitions for VLD

/**
 * @interface  IVLDFieldStateProperty Interface of one property of IVLDField. Always returns boolean.
 */
export interface IVLDFieldStateProperty {
  [propertyName: string]: boolean;
}

/**
 * @interface  IVLDFieldState
 */
export interface IVLDFieldState {
  hasValue: boolean;
  hasError: boolean;
  isPristine: boolean;
  isFocused: boolean;
  isValid: boolean;
}

export interface IVLDForm {
  $form: HTMLFormElement;
  vldFields: IVLDField[];
}

/**
 * @interface  IVLDField - Extended export interface which provides fields with extra properties.
 * @extends {HTMLInputElement} - Inherit input element's properties.
 */
export interface IVLDField extends HTMLInputElement {
  vldForm: IVLDForm;
  $parent: HTMLElement;
  updateState: { (updatedState: IVLDFieldStateProperty): IVLDField };
  removeValidationRules: any;
  applyValidators: any;
  state: IVLDFieldState;
}

export interface IVLDMountedForms {
  [formName: string]: IVLDForm;
}
