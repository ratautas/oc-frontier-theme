import Tweener from '../modules/Tweener';

class TweenerPlane extends Tweener {
  public addTweens() {
    super.addTweens(); // Inherit methods form parent class.
    this.addTween(this.$tweener, 0.5, { height: 0 });
    this.addTween(this.$tweener, 0.83, { width: 2 });
    return this;
  }
}

export default TweenerPlane;
