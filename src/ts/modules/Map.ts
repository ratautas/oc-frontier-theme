import mapStyles from '../misc/mapStyles';

class Map {
  // Initialized map object.
  public map: any;
  // Unique name for the map.
  public name: string;
  // An array of infoWindows.
  public infoWindows: any[] = [];
  // An array of markers.
  public markers: any[] = [];
  /**
   * Map settings (printed from backend).
   * ```JS
   * window['maps'][UNIQUE_MAP_NAME_AS_IN_DATA_ATTRIBUTE] = {
   *   zoom: number,
   *   center_lat: number,
   *   center_lng: number,
   *   markers: [{
   *     lat: number,
   *     lng: number,
   *     content: string,
   *   }]
   *  }
   * ```
   */
  public mapsData: any = null;
  // Zoom level with default value.
  public zoom: number = 10;
  // Center Latitude with default value.
  public centerLat: number = 56;
  // Center Longitude with default value.
  public centerLng: number = 24;
  // Default pin icon url
  public defaultPin: string;

  public $map: HTMLElement;
  public mapsController: any;
  public i: number;
  public gMaps: any;

  /**
   * Creates an instance of Map. Assigns class variables and iniaitate gMaps module.
   * @param {HTMLElement} $map HTML element of the Map's target.
   * @param {*} gMaps Imported Google Maps module.
   * @param {*} mapsController Map's parent initialization function.
   * @param {number} i Map's initializator's place in array.
   * @memberof Map
   */

  constructor($map: HTMLElement, gMaps: any, mapsController: any, i: number) {
    this.$map = $map;
    this.gMaps = gMaps;
    this.mapsController = mapsController;
    this.i = i;

    this.name = $map.dataset.map;

    if (this.name.length) {
      this.mapsData = window['maps'][this.name];
      if (this.mapsData) {
        this.defaultPin = this.renderPin(this.mapsData['pin']);

        if (this.mapsData['zoom']) {
          this.zoom = parseFloat(this.mapsData['zoom']);
        }
        if (this.mapsData['center_lat']) {
          this.centerLat = parseFloat(this.mapsData['center_lat']);
        }
        if (this.mapsData['center_lng']) {
          this.centerLng = parseFloat(this.mapsData['center_lng']);
        }

        this.map = new gMaps.Map(this.$map, {
          zoom: this.zoom,
          center: { lat: this.centerLat, lng: this.centerLng },
          styles: mapStyles,
        });

        this.afterMount();
      } else {
        console.error(`window[maps]['${this.name}'] was not provided for data-map="${this.name}"`);
      }
    } else {
      console.error('Please provide an attribute for [data-map]');
    }
  }

  /**
   * Extra function to call after initialization.
   *
   * @returns {Map} For chaining.
   * @memberof Map
   */
  public afterMount() {
    if (this.mapsData['markers'] && this.mapsData['markers'].length) this.addMarkers();
    return this;
  }

  /**
   * Add markers from backend locations.
   *
   * @returns {Map} For chaining.
   * @memberof Map
   */
  public addMarkers() {
    this.openInfoWindow = this.openInfoWindow.bind(this);

    this.markers = this.mapsData['markers'].map((marker, i) => {
      const markerPosition = new this.gMaps.LatLng(marker.lat, marker.lng);
      const pin = this.renderPin(marker.pin) || this.defaultPin || null;
      const mapsMarker = new this.gMaps.Marker({
        position: markerPosition,
        map: this.map,
        icon: pin,
      }).addListener('click', () => this.openInfoWindow(i));

      const mapsInfoWindow = marker.content
        ? new this.gMaps.InfoWindow({ content: this.renderInfoWindow(marker.content) })
        : null;

      return {
        marker: mapsMarker,
        infoWindow: mapsInfoWindow,
      };
    });
    return this;
  }

  /**
   * Convert marker's content into HTML.
   * Should be overriden depending on project.
   *
   * @returns A string of HTML to be rendered into infoWindow.
   * @memberof Map
   */
  public renderInfoWindow(content) {
    const url = content && Object.keys(content) ? Object.keys(content)[0] : '';

    const html = `
      <div class="infowindow">
        <div class="infowindow__media">
          <img src="${url}" class="infowindow__image" />
        </div>
      </div>
    `;
    return html;
  }

  /**
   * If possible, converts anything to a URL string.
   * Should be overriden depending on project.
   *
   * @returns Url for a pin.
   * @memberof Map
   */
  public renderPin(pin) {
    const url = pin ? Object.keys(pin)[0] || null : null;
    return url;
  }

  /**
   * Loop through all the infoWindows and close them.
   * Open the one which's `i` index matches inner loops' `m` index.
   *
   * @returns google options object
   * @memberof Map
   */
  public openInfoWindow(i) {
    this.markers.forEach((marker, m) => {
      if (marker.infoWindow) {
        m === i ? marker.infoWindow.open(this.map, marker.marker.l) : marker.infoWindow.close();
      }
    });
  }
}

export default Map;
