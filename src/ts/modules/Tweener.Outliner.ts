import Tweener from '../modules/Tweener';

class TweenerOutliner extends Tweener {
  public $fill: HTMLSpanElement;
  public addTweens() {
    this.$fill = document.createElement('span');
    this.$fill.innerHTML = this.$tweener.innerHTML;
    this.$fill.classList.add('outliner__fill');
    this.$tweener.appendChild(this.$fill);
    this.addTween(this.$fill, 1, { height: 0 });
    return this;
  }

  public onEnd() {
    super.onEnd(); // Inherit methods form parent class.
    // this.$tweener.classList.add('outliner__fill');
    // this.$tweener.removeChild(this.$fill);
    return this;
  }
}

export default TweenerOutliner;
