const pkg = require('../package.json');

const fs = require('fs');
const glob = require('glob');
const path = require('path');

const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFileWebpackPlugin = require('write-file-webpack-plugin');
const ImageminWebpackPlugin = require('imagemin-webpack-plugin').default;
const WebappWebpackPlugin = require('webapp-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin');

const ImageminWebpWebpackPlugin = require("imagemin-webp-webpack-plugin");
const StyleLintPlugin = require('stylelint-webpack-plugin');
const PolyfillInjectorPlugin = require('webpack-polyfill-injector');
// // https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');

const hasJquery = require('./utils/hasJquery');

module.exports = function generatePlugins(mode) {
  const plugins = [
    // new webpack.optimize.LimitChunkCountPlugin({
    //   maxChunks: 1,
    // }),
    new HardSourceWebpackPlugin(),
    // new ImageminWebpWebpackPlugin(),
    new CopyWebpackPlugin([{
        cache: true,
        from: '../src/img/**/*',
        to: './img',
        transformPath: (targetPath) => targetPath.replace('src/img/', '')
      }, {
        cache: true,
        // from: '../src/svg/**/*',
        from: './src/svg/**/*',
        to: './img',
        transformPath: (targetPath) => targetPath.replace('src/svg/', '')
      },
      // {
      //   cache: true,
      //   from: '../../storage/app/media',
      //   to: path.resolve(__dirname, '../../../../storage/app/media/optimized'),
      //   test: /\.(png|jpe?g|gif|svg|webp)$/i
      // }
    ]),
    new MiniCssExtractPlugin({
      filename: `css/${mode === 'legacy' ? 'app--legacy' : 'app'}.css`,
    }),
    new ImageminWebpackPlugin({
      // test: /\.(jpe?g|png|gif|svg|webp)$/i,
      test: /\.(jpe?g|png|gif|webp)$/i,
      options: {
        plugins: [
          require('imagemin-gifsicle')({
            interlaced: true,
            optimizationLevel: 3
          }),
          require('imagemin-mozjpeg')({
            quality: 65,
            progressive: true,
          }),
          require('imagemin-optipng')({
            interlaced: true,
            optimizationLevel: 3
          }),
          // require('imagemin-svgo')({
          //   plugins: [{
          //       convertPathData: false
          //     }, {
          //       cleanupIDs: false
          //     }
          //   ]
          // }),
        ]
      }
    }),
  ];
  if (mode === 'development') {
    plugins.push(
      new StyleLintPlugin({
        context: path.resolve(__dirname, '../'),
      }),
      new WriteFileWebpackPlugin(),
    );
  } else {
    plugins.push(
      new WebappWebpackPlugin({
        cache: true,
        // prefix: 'img/webapp/',
        outputPath: '/img/webapp/',
        logo: './src/img/favicon.png', // svg works too!
        favicons: {
          appName: pkg.name || 'app',
          appDescription: pkg.description || 'App Description',
          developerName: pkg.author || 'Me',
          background: pkg.background || '#ddd',
          theme_color: pkg.color || '#333',
          start_url: '/',
          // icons: {
          //   android: false, // Create Android homescreen icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   appleIcon: false, // Create Apple touch icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   appleStartup: false, // Create Apple startup images. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   coast: false, // Create Opera Coast icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   favicons: true, // Create regular favicons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   firefox: false, // Create Firefox OS icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   windows: false, // Create Windows 8 tile icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   yandex: false // Create Yandex browser icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          // }
        }
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          map: {
            inline: false,
            annotation: true,
          },
        },
      }),
      // new CompressionPlugin({
      //   test: /\.(css|js|svg|ttf|eot|woff|woff2)$/
      // }),
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: true
      }),
      // new CleanWebpackPlugin(),
      // new WorkboxWebpackPlugin.GenerateSW({
      //   // these options encourage the ServiceWorkers to get in there fast 
      //   // and not allow any straggling "old" SWs to hang around
      //   clientsClaim: true,
      //   skipWaiting: true
      // })
    )
    if (hasJquery()) {
      plugins.push(new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }));
    }
  }

  return plugins;
};
