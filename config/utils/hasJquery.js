const fs = require('fs');
const getPath = require('./getPath');

let installed = false;
try {
  fs.accessSync(getPath('node_modules/jquery'));
  installed = true;
} catch (err) {
  installed = false;
}

module.exports = function hasJquery() {
  return installed;
};
