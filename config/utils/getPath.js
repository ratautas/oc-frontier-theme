const path = require('path');

const getPath = targetPath => path.resolve(__dirname, `../../${targetPath}`);

module.exports = getPath;
