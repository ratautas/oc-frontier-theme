module.exports = function processFonts(mode) {
  return {
    test: /\.ttf|eot|woff|woff2$/i,
    use: [{
      loader: 'file-loader',
      options: {
        name: 'fonts/[name].[ext]',
      },
    }],
  };
}
