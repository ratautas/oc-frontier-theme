const pkg = require('../package.json');
const hasJquery = require('./utils/hasJquery');

module.exports = function processScripts(mode) {
  const babelPresets = [
    ['@babel/preset-env', {
      modules: false,
      // debug: true,
      corejs: 3,
      useBuiltIns: 'usage',
      targets: {
        browsers: mode !== 'legacy' ? Object.values(pkg.browserslist.modern) : Object.values(pkg.browserslist.legacy),
      },
    }],
  ];

  const babelPlugins = [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/transform-runtime',
    '@babel/plugin-proposal-class-properties'
  ];

  const scriptRules = [{
    test: /\.tsx?$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'awesome-typescript-loader',
    options: {
      sourceMaps: true,
      babelCore: '@babel/core',
      babelOptions: {
        babelrc: true,
        plugins: babelPlugins,
        presets: babelPresets,
      },
      configFileName: './.config/tsconfig.json',
      useBabel: true,
    },
  }, {
    test: /\.jsx?$/,
    exclude: /(node_modules|bower_components|modules\/system)/,
    use: {
      loader: 'babel-loader',
      options: {
        sourceMaps: true,
        cacheDirectory: true,
        plugins: babelPlugins,
        presets: babelPresets,
      },
    },
  }];
  if (hasJquery()) {
    scriptRules.push({
      test: require.resolve('jquery'),
      use: [{
          loader: 'expose-loader',
          options: 'jQuery'
        },
        {
          loader: 'expose-loader',
          options: '$'
        }
      ],
    });
  }

  if (mode !== 'development') {} else {
    scriptRules.push({
      test: /\.jsx?$/,
      loader: 'eslint-loader',
      enforce: 'pre',
      // exclude: /(node_modules|bower_components|modules)/,
      exclude: /(node_modules|bower_components|modules\/system|vendor|october)/,
      options: {
        cache: true,
        fix: false
        // fix: true
      },
    }, {
      test: /\.tsx?$/,
      loader: 'tslint-loader',
      enforce: 'pre',
      exclude: /(node_modules|bower_components|vendor|october)/,
      options: {
        cache: true,
        // cache: false,
        fix: false,
        configFile: './.config/tslint.json',
      },
    });
  }

  return scriptRules;
};
