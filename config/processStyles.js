/* eslint global-require: 0 */

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Fiber = require('fibers');

const pkg = require('../package.json');

module.exports = function processStyles(mode) {
  const styleRules = [
    // mode !== 'development' ? MiniCssExtractPlugin.loader : 'style-loader',
    // MiniCssExtractPlugin.loader,
    {
      loader: MiniCssExtractPlugin.loader,
      options: {
        // you can specify a publicPath here
        // by default it uses publicPath in webpackOptions.output
        publicPath: '../',
        hmr: mode === 'development',
      },
    },
    {
      loader: 'css-loader',
      options: {
        sourceMap: true,
      },
    },
  ];
  if (mode !== 'development') {
    styleRules.push({
      loader: 'postcss-loader',
      options: {
        sourceMap: true,
        ident: 'postcss',
        plugins: () => [
          require('postcss-preset-env')({
            overrideBrowserslist: mode === 'legacy' ? pkg.browserslist.legacy : pkg.browserslist.modern,
          }),
        ],
      },
    }, {
      loader: 'group-css-media-queries-loader',
      options: {
        sourceMap: true,
      },
    });
  }
  styleRules.push({
    loader: 'sass-loader',
    options: {
      sourceMap: true,
      implementation: require('sass'),
      fiber: Fiber,
    },
  });

  return {
    test: /\.s?[ac]ss$/,
    exclude: /(node_modules|bower_components)/,
    use: styleRules,
  };
};
