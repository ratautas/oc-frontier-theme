module.exports = {
  extends: 'tslint-config-airbnb',
  rules: {
    "align": [true, "parameters"]
  }
};
