const sane = require('sane');
const ENV = require('dotenv').config({
  path: '../../.env',
}).parsed;

const PORT = 9000;
const URL = ENV ? ENV.APP_URL : '';
const HOST = URL ? URL.replace('http://', '') : 'localhost';

module.exports = function setupDevServer(mode) {
  return {
    overlay: true,
    before(app, server) {
      sane('./', {
        glob: ['**/*.htm'],
        ignored: /(node_modules|bower_components)/,
      }).on('change', () => {
        server.sockWrite(server.sockets, 'content-changed');
      });
    },

    // publicPath: URL ? `${URL}:${PORT}` : '/',
    publicPath: URL,

    // hot: true,
    hot: false,

    // writeToDisk: true,

    host: HOST,
    port: PORT,
    proxy: {
      '/': URL,
    },
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
}
