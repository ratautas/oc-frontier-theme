/* eslint global-require: 0 */

module.exports = function processImages(mode) {
  return {
    // test: /\.(png|jpe?g|gif|svg|webp)$/i,
    test: /\.(png|jpe?g|gif|webp)$/i,
    exclude: /(node_modules|bower_components)/,
    use: [{
      loader: 'file-loader',
      options: {
        name: 'img/[name].[ext]'
      }
    }, {
      loader: 'img-loader',
      options: {
        plugins: [
          require('imagemin-gifsicle')({
            interlaced: true,
          }),
          require('imagemin-mozjpeg')({
            progressive: true,
            arithmetic: false,
          }),
          require('imagemin-optipng')({
            optimizationLevel: 5,
          }),
          // require('imagemin-svgo')({
          //   plugins: [{
          //     cleanupIDs: false,
          //   }, {
          //     convertPathData: false,
          //   }],
          // }),
        ],
      },
    }]
  };
}
